#!/bin/bash

# DEPENDENCIES
PIPENV=$(whereis -b pipenv | cut -d ' ' -f 2)
# COLOURS
ESC_SEQ="\x1b["
C_RESET="${ESC_SEQ}39;49;00m"
C_RED="${ESC_SEQ}31;01m"
C_GREEN="${ESC_SEQ}32;01m"
C_YELLOW="${ESC_SEQ}33;01m"
C_BLUE="${ESC_SEQ}34;01m"
C_MAGENTA="${ESC_SEQ}35;01m"
C_CYAN="${ESC_SEQ}36;01m"

# CHECK DEPENDENCIES
if ! [ -x ${PIPENV} ]; then
    echo -e ${C_RED}"${PIPENV} ERROR! Hint: sudo pip install pipenv"${C_RESET}
    exit 1
fi

# BANDIT security check
${PIPENV} run bandit -q *.py pira/*.py test/*.py; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Bandit ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# FLAKE8 code style check
${PIPENV} run flake8 --max-line-length=120 --per-file-ignores="setup.py:E501" *.py pira/*.py test/*.py; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Flake8 ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# PIPENV check PEP 508 and package safety
${PIPENV} check --quiet --output bare; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${PIPENV} check ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# PYTHON UNITTEST suite
${PIPENV} run python -m unittest discover; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Python unittest ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

echo -e "Quality assurance: "${C_GREEN}"100%"${C_RESET}
exit 0
