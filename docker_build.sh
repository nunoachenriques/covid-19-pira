#!/bin/bash

# DEPENDENCIES
PIPENV=$(whereis -b pipenv | cut -d ' ' -f 2)
DOCKER=$(whereis -b docker | cut -d ' ' -f 2)
# COLOURS
ESC_SEQ="\x1b["
C_RESET="${ESC_SEQ}39;49;00m"
C_RED="${ESC_SEQ}31;01m"
C_GREEN="${ESC_SEQ}32;01m"
C_YELLOW="${ESC_SEQ}33;01m"
C_BLUE="${ESC_SEQ}34;01m"
C_MAGENTA="${ESC_SEQ}35;01m"
C_CYAN="${ESC_SEQ}36;01m"

# CHECK DEPENDENCIES
if ! [ -x ${PIPENV} ]; then
    echo -e ${C_RED}"${PIPENV} ERROR! Hint: pip install pipenv"${C_RESET}
    exit 1
fi
if ! [ -x ${DOCKER} ]; then
    echo -e ${C_RED}"${DOCKER} ERROR! Hint: https://docs.docker.com/engine/install/"${C_RESET}
    exit 1
fi

# QUALITY ASSURANCE requirement
./qa.sh; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Quality assurance (qa.sh) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

# BUILD
${PIPENV} lock -r > requirements.txt; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${PIPENV} ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
VERSION=$(cat VERSION | tr -d [:space:])
NAME=$(basename -s .git $(git config --get remote.origin.url))
TAG="${CONTAINER_REGISTRY}${CONTAINER_REGISTRY_GROUP}${NAME}:${VERSION}"
${DOCKER} build --label version=${VERSION} -t ${TAG} .; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${DOCKER} build ${TAG} ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
rm -f requirements.txt; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"rm ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

echo -e "Docker build ${TAG}: "${C_GREEN}"100%"${C_RESET}
exit 0
