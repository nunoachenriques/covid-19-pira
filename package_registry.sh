#!/bin/bash

# DEPENDENCIES
PIPENV=$(whereis -b pipenv | cut -d ' ' -f 2)
# COLOURS
ESC_SEQ="\x1b["
C_RESET="${ESC_SEQ}39;49;00m"
C_RED="${ESC_SEQ}31;01m"
C_GREEN="${ESC_SEQ}32;01m"
C_YELLOW="${ESC_SEQ}33;01m"
C_BLUE="${ESC_SEQ}34;01m"
C_MAGENTA="${ESC_SEQ}35;01m"
C_CYAN="${ESC_SEQ}36;01m"

# PACKAGE REGISTRY                   *** CHANGE HERE ***
PROJECT_ID=18578725
REGISTRY=https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/pypi
REGISTRY_USER="nunoachenriques"
REGISTRY_TOKEN=$(cat ${HOME}/.gitlab_api_token.secret | tr -d [:space:])

# SETUP
${PIPENV} run pipenv-setup sync
# QA CODE
./qa.sh; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Quality assurance (qa.sh) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# CLEAN
rm -rf build/ dist/ *.egg-info
# DIST
${PIPENV} run python setup.py sdist bdist_wheel; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Setup distribution ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# QA WHEEL
${PIPENV} run check-wheel-contents dist/*.whl; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Quality assurance (check-wheel-contents) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
${PIPENV} run twine check dist/*.whl; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Quality assurance (twine check) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
# UPLOAD
${PIPENV} run twine upload --username ${REGISTRY_USER} --password ${REGISTRY_TOKEN} --repository-url ${REGISTRY} dist/*.whl; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Package upload (twine upload) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

echo -e "Package registry ${TAG}: "${C_GREEN}"100%"${C_RESET}
exit 0
