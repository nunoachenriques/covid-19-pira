#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``ws`` module.
This is an integration test.
"""

import json
import logging
import unittest

from pira.ws import app

app.testing = True
app.logger.setLevel(logging.CRITICAL)


class TestWs(unittest.TestCase):

    def test_ws(self):
        with app.test_client() as client:
            result = client.get("/")
            self.assertIn(b'COVID-19 Population Indicator Analysis Assessment', result.data)
            result = client.get("/html?select=risk == 1.0")
            self.assertIn(b'| 1 locations |', result.data)
            result = client.get("/json?select=risk == 1.0")
            self.assertIn(b'"risk":1.0', result.data)
            j = json.loads(result.data)
            self.assertEqual(1, len(j), "JSON request FAILED!")
            result = client.get("/json?select=no_column == 1.0")
            self.assertEqual(400, result.status_code, "JSON no_column FAILED!")


if __name__ == "__main__":
    unittest.main()
