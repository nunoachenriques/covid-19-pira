#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``analysis`` module
"""

import unittest

import numpy as np
import pandas as pd

from pira.analysis import Analysis


class TestAnalysis(unittest.TestCase):

    # active, deaths, recovered, tests, population
    _test_data = pd.DataFrame().from_dict(
        {
            "UNKNOWN 0 T0": [0, 0, 0, 0, 0, 0],
            "UNKNOWN R100 T0": [100, 0, 0, 100, 0, 0],
            "UNKNOWN A1M T0": [1e6, 1e6, 0, 0, 0, 0],
            "HIGH A1M T1k": [1e6, 1e6, 0, 0, 1e3, 1e7],
            "HIGH A1M T1M": [1e6, 1e6, 0, 0, 1e6, 1e7],
            "HIGH A100 T1k": [100, 100, 0, 0, 100, 1e7],
            "HIGH A25D25R50 T10k": [100, 25, 25, 50, 100, 1e7],
            "HIGH R100 T1k": [100, 0, 0, 100, 100, 1e7],
            "HIGH A100 T1M P10M": [100, 100, 0, 0, 1e6, 1e7],
            "MEDIUM A100 T1M P1M": [100, 100, 0, 0, 1e6, 1e6],
            "MEDIUM A25D25R50 T1M": [100, 25, 25, 50, 1e6, 1e7],
            "LOW R100 T1M": [100, 0, 0, 100, 1e6, 1e7],
            "LOW R10 T1M": [10, 0, 0, 10, 1e6, 1e7],
            "LOW R1 T1M": [1, 0, 0, 1, 1e6, 1e7],
            "SAFE": [0, 0, 0, 0, 1e6, 1e7]
        },
        orient="index",
        columns=["cases", "active", "deaths", "recovered", "tests", "population"]
    )
    _risk_expected = [np.nan, np.nan, np.nan,
                      1.0, 0.812500, 0.562500, 0.488418, 0.343750, 0.312500,
                      0.250000, 0.238418, 0.093750, 0.078125, 0.062500, 0.0]
    _rank_expected = [np.nan, np.nan, np.nan,
                      100.0, 92.0, 83.0, 75.0, 67.0, 58.0,
                      50.0, 42.0, 33.0, 25.0, 17.0, 8.0]

    def test_init(self):
        with self.assertRaises(ValueError):
            Analysis(self._test_data.drop(columns=["active"]))
        self.assertIsInstance(Analysis(self._test_data), Analysis, f"Analysis({self._test_data}) FAILED!")

    def test_risk(self):
        df = Analysis(self._test_data).risk().data
        risk = np.round(df["risk"], 6).tolist()
        self.assertNotEqual(self._risk_expected[:3], risk[:3], "Risk FAILED!")
        self.assertEqual(self._risk_expected[3:], risk[3:], "Risk FAILED!")

    def test_rank(self):
        df = self._test_data
        df["risk"] = self._risk_expected
        df = Analysis(df).rank().data
        rank = df["rank"].tolist()
        self.assertNotEqual(self._rank_expected[:3], rank[:3], "Rank FAILED!")
        self.assertEqual(self._rank_expected[3:], rank[3:], "Rank FAILED!")


if __name__ == "__main__":
    unittest.main()
