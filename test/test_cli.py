#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``cli`` module.
This is an integration test.
"""

import os
import subprocess  # nosec | Avoids Bandit B404 check
import sys
import unittest

from pira.cli import Cli


class TestCli(unittest.TestCase):

    def test_bootstrap_run(self):
        with self.assertRaises(SystemExit) as cm:
            # simulate argument from command line
            sys.argv[1] = os.path.join(os.path.dirname(__file__), "test_data.csv")
            Cli().bootstrap().run(output=False)
        self.assertTrue(cm.exception.code == 0, "CLI FAILED!")

    def test_cli(self):
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py", os.path.join(os.path.dirname(__file__), "test_data.csv")],
                                capture_output=True)
        self.assertEqual(0, result.returncode, f"Return code [{result.returncode}] FAILED!")
        output_expected = b'\nGuide: NaN = UNKNOWN risk due to unreported values.' \
                          b'\n\n                location    cases   active  ...  population      risk   rank' \
                          b'\n1           HIGH A1M T1k  1000000  1000000  ...    10000000  1.000000  100.0' \
                          b'\n2           HIGH A1M T1M  1000000  1000000  ...    10000000  0.812500   92.0' \
                          b'\n3          HIGH A100 T1k      100      100  ...    10000000  0.562500   83.0' \
                          b'\n4    HIGH A25D25R50 T10k      100       25  ...    10000000  0.488418   75.0' \
                          b'\n5          HIGH R100 T1k      100        0  ...    10000000  0.343750   67.0' \
                          b'\n6     HIGH A100 T1M P10M      100      100  ...    10000000  0.312500   58.0' \
                          b'\n7    MEDIUM A100 T1M P1M      100      100  ...     1000000  0.250000   50.0' \
                          b'\n8   MEDIUM A25D25R50 T1M      100       25  ...    10000000  0.238418   42.0' \
                          b'\n9           LOW R100 T1M      100        0  ...    10000000  0.093750   33.0' \
                          b'\n10           LOW R10 T1M       10        0  ...    10000000  0.078125   25.0' \
                          b'\n11            LOW R1 T1M        1        0  ...    10000000  0.062500   17.0' \
                          b'\n12                  SAFE        0        0  ...    10000000  0.000000    8.0' \
                          b'\n13          UNKNOWN 0 T0        0        0  ...           0       NaN    NaN' \
                          b'\n14       UNKNOWN R100 T0      100        0  ...           0       NaN    NaN' \
                          b'\n15        UNKNOWN A1M T0  1000000  1000000  ...           0       NaN    NaN' \
                          b'\n\n[15 rows x 9 columns]\n'
        # Strip FIRST line of the output because of the test_data.csv full path
        self.assertEqual(output_expected, b'\n'.join(result.stdout.split(b'\n')[2:]), "Output FAILED!")
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py", "-s", "index == 'MEDIUM A25D25R50 T1M'",
                                 os.path.join(os.path.dirname(__file__), "test_data.csv")],
                                capture_output=True)
        output_expected = b'\nGuide: NaN = UNKNOWN risk due to unreported values.' \
                          b'\n\n               location  cases  active  ...  population      risk  rank' \
                          b'\n1  MEDIUM A25D25R50 T1M    100      25  ...    10000000  0.238418  42.0' \
                          b'\n\n[1 rows x 9 columns]\n'
        # Strip FIRST line of the output because of the test_data.csv full path
        self.assertEqual(output_expected, b'\n'.join(result.stdout.split(b'\n')[2:]), "Output FAILED!")


if __name__ == "__main__":
    unittest.main()
