#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``data`` module
"""

import logging
import os
import unittest

from pandas import DataFrame

from pira.data import Data


class TestData(unittest.TestCase):

    _test_data = ["nunoachenriques.net",  # NotImplementedError
                  "https://www.worldometers.info/coronavirus/",  # OK
                  os.path.join(os.path.dirname(__file__), "test_data.csv")]  # OK

    def test_init(self):
        with self.assertRaises(NotImplementedError):
            Data(self._test_data[0])
        self.assertIsInstance(Data(self._test_data[1]), Data, f"Data({self._test_data[1]}) FAILED!")
        self.assertIsInstance(Data(self._test_data[2]), Data, f"Data({self._test_data[2]}) FAILED!")

    def test_fetch(self):
        # Six columns: ["cases", "active", "deaths", "recovered", "tests", "population"]
        logging.getLogger().setLevel(logging.CRITICAL)
        df: DataFrame = Data(self._test_data[2]).fetch()
        self.assertTupleEqual((15, 6), df.shape, f"Fetch {self._test_data[2]} FAILED!")
        df: DataFrame = Data(self._test_data[1]).fetch()
        self.assertTrue(df.shape[0] > 0 and df.shape[1] == 6, f"Fetch {self._test_data[1]} FAILED!")


if __name__ == "__main__":
    unittest.main()
