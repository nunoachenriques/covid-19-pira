FROM python:3.8-slim-buster
# No Python .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1
# No buffering for easier container logging
ENV PYTHONUNBUFFERED 1
ENV APP_PATH /app
ENV APP_USER appuser

LABEL name="COVID-19 PIRA" \
      description="COVID-19 Population Indicator Risk Assessment" \
      maintainer="nunoachenriques.net"

# Required for building the uWSGI wheel
RUN apt-get update \
    && apt-get install -y gcc libpcre3 libpcre3-dev --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false

WORKDIR $APP_PATH
COPY requirements.txt $APP_PATH
RUN python -m pip install --no-cache-dir -r requirements.txt
COPY . $APP_PATH

# Switch to a non-root user
RUN useradd $APP_USER && chown -R $APP_USER $APP_PATH
USER $APP_USER

# Flask application deployed to a WSGI server (uWSGI)
# NOTICE the EXPOSE 5000 and the CMD "--http=:5000". Adapt accordingly...
EXPOSE 5000
CMD ["uwsgi", "--http=:5000", "--wsgi-file=/app/pira/ws.py", "--callable=app", "--processes=2", "--master"]
