# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open("README.md") as f:
    README = f.read()

with open("VERSION") as f:
    VERSION = f.read()

setup(
    python_requires=">=3.7, <3.9",
    install_requires=[
        "beautifulsoup4==4.9.3",
        "certifi==2020.11.8",
        "chardet==3.0.4",
        "click==7.1.2; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'",
        "flask==1.1.2",
        "idna==2.10; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "itsdangerous==1.1.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "jinja2==2.11.2; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'",
        "lxml==4.6.1",
        "markupsafe==1.1.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "numpy==1.19.4",
        "pandas==1.1.4",
        "python-dateutil==2.8.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "pytz==2020.4",
        "requests==2.24.0",
        "six==1.15.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "soupsieve==2.0.1; python_version >= '3.0'",
        "tqdm==4.51.0",
        "urllib3==1.25.11; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4' and python_version < '4'",
        "uwsgi==2.0.19.1",
        "werkzeug==1.0.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'",
    ],
    dependency_links=[],
    packages=find_packages(
        where=".", include=["pira"], exclude=["build", "dist", "test", "test.*"]
    ),
    package_data={"pira": ["templates/*"]},
    name="covid-19-pira",
    version=VERSION,
    author="Nuno A. C. Henriques",
    author_email="nunoachenriques@gmail.com",
    description="COVID-19 Population Indicator Risk Assessment."
    " Compute a custom ad hoc personal risk and ranking metric of"
    " the locations affected by the COVID-19 disease based on the"
    " assumption that actives cases are exponentially relevant,"
    " reduced by the recovered ones, and increased by lack of"
    " detection tests all relative to population total.",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nunoachenriques/covid-19-pira",
    license="Apache-2.0",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
)
