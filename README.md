[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")](http://www.apache.org/licenses/LICENSE-2.0.html)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/nunoachenriques/covid-19-pira)](https://gitlab.com/nunoachenriques/covid-19-pira)

# COVID-19 Population Indicator Risk Assessment

Compute a custom ad hoc personal risk and ranking metric of the locations
affected by the COVID-19 disease based on the assumption that actives cases are
exponentially relevant, reduced by the recovered ones, and increased by lack
of detection tests all relative to population total. For more details including
the mathematics refer to `Analysis()` in `pira/analysis.py`.

The ranking is obtained by making use of a percentile rank over risk value and
presented as percentage, i.e., the region with the highest risk get 100, the
others descend towards 0.

**CAVEAT:**  It could be much better by making use of more factors, such as
(a) smaller regions (counties or provinces) with population density;
(b) time-series analysis of active cases.

**NOTICE:** This is a personal exercise and NOT endorsed by any health
 organisation. Everyone should TRUST and FOLLOW the GUIDELINES from local HEALTH
 AUTHORITIES and from the [World Health Organisation (WHO)](https://www.who.int/).

**GUIDE:** A location with risk and rank as `NaN` due to some unreported data
(active, deaths, recovered, tests, population) means an UNKNOWN risk.
Moreover, one may consider unknown to be the highest possible risk regarding
COVID-19 contagious.

## Pip

Install as package using `pip`. It is recommended that you first create a
self-contained Python environment (e.g., `pipenv --python 3.8`).

```bash
pip install covid-19-pira --extra-index-url https://gitlab.com/api/v4/projects/18578725/packages/pypi/simple
```

## Develop

https://gitlab.com/nunoachenriques/covid-19-pira

### Fork

In order to facilitate pull requests if you want to contribute then go to
https://gitlab.com/nunoachenriques/covid-19-pira and hit **fork**!

### Install

```bash
git clone git@gitlab.com:{YOUR_OWN_USER}/covid-19-pira.git
```

In order to keep your system's Python untainted, `covid-19-pira` is contained
in a virtual environment. Therefore, install `pipenv` for the environment
management --- the script `qa.sh` make use of it and also runs inside
`docker_build.sh` (Section [Web service](#web-service)). Moreover,
the `python3-dev` system package is required by the `uwsgi` package.

```bash
sudo pip install pipenv
sudo apt install python3-dev
```

Inside the `covid-19-pira` project directory, install dependencies, creating the
environment. Moreover, it install `pre-commit` into the project's `.git`
hooks --- it will keep you safe from usual code style or security mistakes when
committing changes.

```bash
pipenv install --dev
pipenv run pre-commit install
```

### Check

There is a command-line **quality assurance** test suite. It provides code
style (`flake8`), security (`bandit`), and unit testing.

```bash
./qa.sh
```

### Run

#### Command-line interface

A command-line interface is available and ready to use by running `app_cli.py`.

```bash
pipenv run python app_cli.py -h
```

![command-line interface run](app_cli_run.png)

#### Web service

A Web service is available and ready to use. Refer below for the options
available to deploy the application. A Web browser pointed to the exposed
address has two endpoints available: `/html` and `/json`. 

#### Flask's **built-in** server

NOTICE: the default serving port 5000 --- `http://localhost:5000`

```bash
pipenv run python app_ws.py
```

#### Flask application deployed to a **WSGI** server ([uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/))

NOTICE: the changed host port 5005 --- `http://localhost:5005`

```bash
pipenv run uwsgi --http=127.0.0.1:5005 --wsgi-file=pira/ws.py --callable=app
```

#### Flask application deployed to a **WSGI** server ([uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/)) inside a **container** ([Docker](https://docs.docker.com/engine/install/))

NOTICE: the changed host port "[...] -p **5005**: [...]" --- `http://localhost:5005`

```bash
./docker_build.sh
docker run -p 5005:5000 --name=covid-19-pira covid-19-pira:1.1.1
```

## Deploy

The application may be served using the uWSGI server behind a reverse proxy,
such as [Apache](https://httpd.apache.org) and [nginx](https://nginx.org/).
Furthermore, the included scripts may be used to build a
[Docker](https://docs.docker.com/engine/install/) container, publish it in an
available registry, and [deploy](https://docs.docker.com/get-started/kube-deploy/)
it in a microservice-style [Kubernetes](https://kubernetes.io/) cluster.

### Your own Docker and registry

A Docker registry custom script `docker_registry.sh` automates the whole process
of building, uploading, and registering the Docker image. Feel free to use it as
inspiration to your own script: change the registry accordingly your own
preference or own registry. Of course, you may still use my
[publicly available Docker image](#the-original-project-docker-image).

**NOTICE**
 1. Inside `docker_registry.sh`:
    1. Change credentials.
    2. Change registry provider.
 2. Update `VERSION`.

```bash
./docker_registry.sh
```

### The original project Docker image

A self-hosted [Docker engine](https://docs.docker.com/engine/install/) may
simplify a staging deployment by making use of an [available ready-to-deploy
Docker image](https://gitlab.com/nunoachenriques/covid-19-pira/container_registry).
For instance, see below how to deploy an available covid-19-pira image version 1.1.1,
serving unless stopped, host port 5005:

```bash
docker run -d --restart=unless-stopped -p 5005:5000 --name=covid-19-pira \
    registry.gitlab.com/nunoachenriques/covid-19-pira:1.1.1
```

## Distribute

Create a Python wheel for the `pira` module to provide easy install and
use. A requirements sync between `Pipfile` and `setup.py` is provided by
`pipenv-setup`. Check the `package_registry.sh` script.

**NOTICE**
 1. Inside `package_registry.sh`:
    1. Change credentials.
    2. Change registry provider.
 2. Update `VERSION`.

```bash
./package_registry.sh
```

Check `dist` directory for the `*.tar.gz` and `*.whl` files.

## History

It all started in the context of the COVID-19 pandemic in 2020.

## License

Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
