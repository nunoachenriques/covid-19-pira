#!/bin/bash

# DEPENDENCIES
DOCKER=$(whereis -b docker | cut -d ' ' -f 2)
# COLOURS
ESC_SEQ="\x1b["
C_RESET="${ESC_SEQ}39;49;00m"
C_RED="${ESC_SEQ}31;01m"
C_GREEN="${ESC_SEQ}32;01m"
C_YELLOW="${ESC_SEQ}33;01m"
C_BLUE="${ESC_SEQ}34;01m"
C_MAGENTA="${ESC_SEQ}35;01m"
C_CYAN="${ESC_SEQ}36;01m"

# CHECK DEPENDENCIES
if ! [ -x ${DOCKER} ]; then
    echo -e ${C_RED}"${DOCKER} ERROR! Hint: https://docs.docker.com/engine/install/"${C_RESET}
    exit 1
fi

# CONTAINER REGISTRY                   *** CHANGE HERE ***
REGISTRY="registry.gitlab.com"
REGISTRY_USER="nunoachenriques"
REGISTRY_TOKEN_FILE="${HOME}/.gitlab_api_token.secret"

# BUILD requirement
export CONTAINER_REGISTRY="${REGISTRY}/"
export CONTAINER_REGISTRY_GROUP="${REGISTRY_USER}/"
./docker_build.sh; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"Docker build (docker_build.sh) ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

# LOGIN > PUSH > LOGOUT
cat ${REGISTRY_TOKEN_FILE} | tr -d [:space:] | ${DOCKER} login -u ${REGISTRY_USER} --password-stdin ${REGISTRY}; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${DOCKER} login ${REGISTRY} ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
VERSION=$(cat VERSION | tr -d [:space:])
NAME=$(basename -s .git $(git config --get remote.origin.url))
TAG="${CONTAINER_REGISTRY}${CONTAINER_REGISTRY_GROUP}${NAME}:${VERSION}"
${DOCKER} push ${TAG}; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${DOCKER} push ${TAG} ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi
${DOCKER} logout ${REGISTRY}; status=$?
if [ ${status} -ne 0 ]; then
    echo -e ${C_RED}"${DOCKER} logout ${REGISTRY} ERROR! Exit status: ${status}!"${C_RESET}
    exit 1
fi

echo -e "Docker registry ${TAG}: "${C_GREEN}"100%"${C_RESET}
exit 0
