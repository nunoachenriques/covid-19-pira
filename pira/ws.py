#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
COVID-19 Population Indicator Analysis Assessment

Web service.

TODO: data_source as an argument (html, json) to support data source options.
"""

from datetime import datetime

from flask import Flask, request, render_template, jsonify, abort, Response
from pandas import DataFrame
from pandas.core.computation.ops import UndefinedVariableError
from requests.exceptions import RequestException

from pira.analysis import Analysis
from pira.data import Data

app = Flask(__name__)
data_source = "https://www.worldometers.info/coronavirus/"
title = "COVID-19 Population Indicator Risk Assessment"
with open("VERSION") as f:
    version = f.read()


@app.route("/", methods=["GET"])
@app.route("/html", methods=["GET"])
@app.route("/json", methods=["GET"])
def pira():
    query = request.args.get("select", None)
    try:
        data: DataFrame = Data(data_source).fetch()
        result: DataFrame = Analysis(data).risk().rank().data.sort_values(by=["rank", "risk"], ascending=False)
        if query is not None:
            result = result.query(query)
        if request.path == "/json":
            return Response(result.to_json(orient="index"), mimetype='application/json'), 200
        else:
            return render_template(
                "pira.jinja",
                title=title,
                message=f"{result.shape[0]} locations | {datetime.utcnow().isoformat()[:16]} UTC",
                data_source=data_source,
                data=result,
                version=version
            ), 200
    except (KeyError, SyntaxError, UndefinedVariableError) as e:
        abort(400, e)
    except RequestException as e:
        abort(424, e)
    except ValueError as e:
        abort(500, e)
    except NotImplementedError as e:
        abort(501, e)


@app.errorhandler(400)
def abort_bad_request(error):
    app.logger.error(error)
    message = "The select query is NOT properly formed! Hint: /html?select=rank < 50"
    if request.path == "/json":
        return jsonify({"message": message, "data_source": data_source}), 400
    else:
        return render_template(
            "error.jinja",
            title=title,
            message=message,
            data_source=data_source,
            error=error
        ), 400


@app.errorhandler(424)
def abort_failed_dependency(error):
    app.logger.error(error)
    message = "Web data source NOT responding!"
    if request.path == "/json":
        return jsonify({"message": message, "data_source": data_source}), 424
    else:
        return render_template(
            "error.jinja",
            title=title,
            message=message,
            data_source=data_source,
            error=error
        ), 424


@app.errorhandler(500)
def abort_internal_server_error(error):
    app.logger.error(error)
    message = "Internal data shape NOT compliant!"
    if request.path == "/json":
        return jsonify({"message": message, "data_source": data_source}), 500
    else:
        return render_template(
            "error.jinja",
            title=title,
            message=message,
            data_source=data_source,
            error=error
        ), 500


@app.errorhandler(501)
def abort_not_implemented(error):
    app.logger.error(error)
    message = "Data source NOT supported!"
    if request.path == "/json":
        return jsonify({"message": message, "data_source": data_source}), 501
    else:
        return render_template(
            "error.jinja",
            title=title,
            message=message,
            data_source=data_source,
            error=error
        ), 501
