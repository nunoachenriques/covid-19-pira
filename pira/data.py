#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
COVID-19 Population Indicator Analysis Assessment

Data resource get and parse for all regions.
"""

import hashlib
import logging
import os
import re
import sys
import tempfile
from datetime import datetime
from urllib.parse import urlsplit

import pandas as pd
import requests
from bs4 import BeautifulSoup
from pandas import DataFrame


class Data(object):
    """
    A custom content resource tool in order to fetch and parse from known
    sources.
    """

    _supported_web_sources = ["www.worldometers.info"]
    _csv_sep = ","

    def __init__(self, source: str):
        """
        Start by checking the source type (a) Web address of supported host
        sources; or (b) path to local file (comma-separated where column 0
        is the index, i.e., contains each row label) created from::

            pandas.DataFrame.to_csv(sep=",")

        :param source: A content source local (CSV) or remote (Web).
        :exception NotImplementedError: On unsupported source or file not found.
        """
        self.is_file_source = os.path.exists(source)
        self.source_host = urlsplit(source).hostname
        if self.is_file_source or (self.source_host in self._supported_web_sources):
            self.source = source
        else:
            raise NotImplementedError(f"{source} NOT SUPPORTED! Hint: check URL or FILE."
                                      f"\n\tSupported Web sources: {self._supported_web_sources}")

    def fetch(self) -> DataFrame:
        """
        Get content from the given source, parse and deliver it into the
        required format for analysis. Manage a simple local cache in order
        to save Web requests and parse processing (the slowest component by
        orders of magnitude). The parsed content cache is updated hourly and
        on-demand (on the first request of the hour UTC).

        :return: Parsed content ready for analysis.
        :exception requests.exceptions.RequestException: On Web request failure.
        """
        if self.is_file_source:
            return pd.read_csv(self.source, sep=self._csv_sep, index_col=0)
        else:  # Web source
            # cache in OS temporary location
            cache_path = os.path.join(tempfile.gettempdir(), "covid-19-pira", "cache")
            os.makedirs(cache_path, mode=0o755, exist_ok=True)
            # cache item name
            source_hash = hashlib.sha256(self.source.encode()).hexdigest()
            today_hour = datetime.utcnow().isoformat()[:13]  # YYYY-MM-DDTHH, i.e., hourly updates
            cache_item = os.path.join(cache_path, f"{source_hash}_{today_hour}")
            if os.path.exists(cache_item):
                # get cache item content
                logging.debug(f"The given {self.source} IS CACHED at {cache_item}")
                return pd.read_csv(cache_item, sep=self._csv_sep, index_col=0)
            else:
                # Web request
                logging.debug(f"The given {self.source} IS NOT CACHED! Requesting...")
                try:
                    response = requests.get(self.source)
                    logging.debug(f"... response: {response.status_code}")
                    if self.source_host == "www.worldometers.info":
                        content = self._parse_worldometers(response.text)
                    else:
                        content = None
                    # update cache
                    if content is not None:
                        content.to_csv(cache_item, sep=self._csv_sep)
                    return content
                except requests.exceptions.RequestException:
                    msg = f"Web request FAILED! Hint: check SOURCE and NETWORK [{self.source}].\n\t{sys.exc_info()[1]}"
                    logging.error(msg, exc_info=False, stack_info=False)
                    raise requests.exceptions.RequestException(msg)

    @staticmethod
    def _parse_worldometers(content: str) -> DataFrame:
        """
        Parse the values from the Web page into the required data frame format.

        :return: The content parsed into a data frame ready for analysis.
        """
        regions_data = dict()
        bs: BeautifulSoup = BeautifulSoup(content, "lxml")
        logging.debug(bs.title.string)
        bs_table = bs.select_one("table#main_table_countries_today")
        # header (columns of interest)
        header = [th.get_text(strip=True) for th in bs_table.thead.tr.find_all("th")]
        logging.debug(header)
        name_i = header.index("Country,Other")
        cases_i = header.index("TotalCases")
        active_i = header.index("ActiveCases")
        deaths_i = header.index("TotalDeaths")
        recovered_i = header.index("TotalRecovered")
        tests_i = header.index("TotalTests")
        population_i = header.index("Population")
        # body (content)
        for tr in bs_table.tbody.find_all("tr", class_=False):  # tr country data has no class
            td = tr.find_all("td")
            regions_data[td[name_i].text] = [
                td[cases_i].text,
                td[active_i].text,
                td[deaths_i].text,
                td[recovered_i].text,
                td[tests_i].text,
                td[population_i].text
            ]
        regions_data = pd.DataFrame().from_dict(
            regions_data,
            orient="index",
            columns=["cases", "active", "deaths", "recovered",
                     "tests", "population"])
        logging.debug(regions_data.to_string())
        re_to_numeric = re.compile(r"[^0-9.]")
        regions_data = (
            regions_data
            .applymap(lambda x: re_to_numeric.sub("", x))  # 3,456.00 -> 3456.00
            .replace("", 0)
            .astype({"cases": "int", "active": "int", "deaths": "int", "recovered": "int",
                     "tests": "int", "population": "int"}))
        logging.debug(regions_data.to_string())
        return regions_data
