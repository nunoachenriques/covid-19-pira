#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
COVID-19 Population Indicator Analysis Assessment

Analysis metrics for all regions in the source frame.
"""

import numpy as np
from pandas import DataFrame, Series


class Analysis(object):
    """
    A custom assessment which contains all regions from given source and
    computes risk and rank by region.
    """

    def __init__(self, data: DataFrame):
        """
        Start by storing the source frame which will be transformed by cleaning
        and adding columns by making use of the available class functions.
        The source frame must have the region as index and also include the
        following columns::

                     cases active deaths recovered  tests population
            Portugal 25100  22500   1000      1600 425000   10000000
            ...

        :param data: A source frame where the index is the region name.
        :exception ValueError: On data shape not compliant.
        """
        self.required_columns = ["cases", "active", "deaths", "recovered", "tests", "population"]
        if sorted(set(data.columns.to_list()).intersection(self.required_columns)) == sorted(self.required_columns):
            self.data = data
        else:
            raise ValueError(f"Data shape FAILED! Hint: comply with the required columns {self.required_columns}")

    def risk(self) -> "Analysis":
        """
        Compute the risk value for every region and update or add a new column
        to the source frame. The risk formula increases with active (A) cases
        relevance exponentially over deaths (D) and it is inversely proportional
        to the recovered (R) cases (C) to the power of 0.25. It is relative to
        population (P) total. It distinguishes regions similar cases value,
        relative to population, by making use of the PIRA component, i.e., risk
        decreases with the increase of tests (T) per population total. Total
        cases (PIRA) is A^2 + D + R^0.25::

            risk = C + PIRA

                A^2 + D   1                                   P
            C = ------- x -      PIRA =  (A^2 + D + R^0.25) x -
                R^0.25    P                                   T

        The risk formula is smoothed by making use of log10 and then normalised
        to [0, 1] where 0 means no risk, conversely, 1 is a maximum threat.
        The NaN means an unknown risk due to reported data, such as zero
        tests::

            risk = log10(C + PIRA)

        :return: This object with the risk column updated for all.
        """
        active: Series = self.data["active"].pow(2)
        recovered: Series = self.data["recovered"].pow(0.25)
        cases_part: Series = \
            (active + self.data["deaths"]) / (np.maximum(recovered, 1) * self.data["population"])
        pira_part: Series = \
            (active + self.data["deaths"] + recovered) * self.data["population"] / self.data["tests"]
        # Risk smoothed with log10
        #  special case replace: inf with NaN; 0 with 1
        #  population = 0 => NaN, tests = 0 => NaN
        self.data["risk"] = np.log10((cases_part + pira_part).replace([np.inf, 0], [np.NaN, 1]))
        # normalise [0, 1]
        self.data["risk"] = (self.data["risk"] - self.data["risk"].min()) / \
                            (self.data["risk"].max() - self.data["risk"].min())
        # Insert RISK = NaN if cases NOT EQUAL to active + deaths + recovered
        self.data["risk"] = self.data["risk"].where(
            self.data["cases"].eq(self.data[["active", "deaths", "recovered"]].sum(axis=1)), np.NaN)
        return self

    def rank(self) -> "Analysis":
        """
        Compute the rank based on the risk value for every region and update or
        add a new column to the source frame.

        :return: This object with the risk-based percentile rank from the
            highest 100 towards the lowest 0.
        """
        self.data["rank"] = (self.data["risk"].rank(pct=True) * 100).round(0)
        return self
