#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
COVID-19 Population Indicator Analysis Assessment

Command-line interface.
"""

import argparse
import logging

import pandas as pd
from pandas import DataFrame
from tqdm.auto import tqdm

from pira.analysis import Analysis
from pira.data import Data


class Cli(object):
    """
    Provides bootstrap and complete integration of application run from
    command-line a Python script, such as:

        from pira.cli import Cli


        if __name__ == "__main__":
            Cli().bootstrap().run()
    """

    def __init__(self):
        """
        Initialisation of the class parameters:

            * self.source: data source local (CSV) or remote (Web) address.
            * self.query: pandas.DataFrame.query() syntax-like string.
        """
        self.source = None
        self.query = None
        with open("VERSION") as f:
            self.version = f.read()

    def bootstrap(self) -> "Cli":
        """
        Required to protect the start up and help the user. Parse the
        command-line arguments to obtain source address and options. Set the
        logging level regarding verbosity requested.

        :return: This instance with self.source = args.source and
            self.query = args.s
        """
        # noinspection PyTypeChecker
        cmd_line_parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
        cmd_line_parser.description = \
            f"COVID-19 Population Indicator Analysis Assessment {self.version}" \
            "\n\nhttps://gitlab.com/nunoachenriques/covid-19-pira" \
            "\n\nNOTICE: This is a personal exercise and NOT endorsed by any health organisation." \
            "\nEveryone should TRUST and FOLLOW the GUIDELINES from local HEALTH AUTHORITIES" \
            "\nand from the World Health Organisation (WHO) at https://www.who.int/."
        cmd_line_parser.add_argument("-v", action="count", default=0,
                                     help="Output verbosity: none, info (-v), debug (-vv).")
        cmd_line_parser.add_argument("-s", metavar="SELECT", type=str,
                                     help="Select from output using pandas.DataFrame.query() syntax.")
        cmd_line_parser.add_argument("source", type=str,
                                     help="The Web address or file path containing the COVID-19 data.")
        cmd_line_parser.epilog = \
            "Usage examples:" \
            "\n\n  Fetch and parse data from a file, output the ranking result:" \
            "\n    pipenv run python app_cli.py test/test_data.csv" \
            "\n\n  Fetch and parse data from a file, select one region from the output:" \
            "\n    pipenv run python app_cli.py -s \"index == 'MEDIUM A25D25R50 T1M'\" test/test_data.csv" \
            "\n\n  Fetch and parse data from worldometers source, show process, output the ranking result:" \
            "\n    pipenv run python app_cli.py -v https://www.worldometers.info/coronavirus/" \
            "\n\n  Fetch and parse data from worldometers source, select low rankings from result:" \
            "\n    pipenv run python app_cli.py -s \"rank < 10\" https://www.worldometers.info/coronavirus/" \
            "\n\n  Fetch and parse data from worldometers source, select UNKNOWN rankings from result:" \
            "\n    pipenv run python app_cli.py -s \"rank == 'NaN'\" https://www.worldometers.info/coronavirus/"
        args = cmd_line_parser.parse_args()
        if args.v == 0:
            logging.getLogger().setLevel(logging.CRITICAL)
        elif args.v == 1:
            logging.getLogger().setLevel(logging.INFO)
        else:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.debug(f"Logging set to {logging.getLevelName(logging.getLogger().level)}")
        self.source = args.source
        self.query = args.s
        return self

    def run(self, output: bool = True):
        """
        Run the application, get data and parse from given source, and output
        the result in rank-descending sorted result. A filter may be applied to
        the result by making use of pandas query syntax
        (e.g., -s "index == 'Portugal'").

        :param output: True for result output.
        """
        with tqdm(total=2, disable=(logging.getLogger().level >= logging.CRITICAL)) as bar:
            bar.set_description_str("COVID-19 PIRA")
            bar.set_postfix_str("Getting and parsing source...")
            df = Data(self.source).fetch()
            bar.update()
            bar.set_postfix_str("Computing risk and rank...")
            analysis = Analysis(df).risk().rank()
            bar.update()
            bar.set_postfix_str("")
        pd.set_option("display.max_rows", None)
        if output:
            result: DataFrame = analysis.data.sort_values(by=["rank", "risk"], ascending=False)
            if self.query is not None:
                result = result.query(self.query)
            # Shift index countries to "location" column; create numbered index starting at 1.
            result = result.reset_index().rename(columns={"index": "location"})
            result.index = range(1, 1 + len(result))
            print(f"\nPIRA {self.version} | Data source: {self.source}"
                  "\n\nGuide: NaN = UNKNOWN risk due to unreported values."
                  f"\n\n{result}")
        raise SystemExit(0)
